/** @format */

import {AppRegistry, I18nManager} from 'react-native';
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

// import ApiPlayground from './src/screens/ApiPlayground';
import Root from './src/routes'
I18nManager.allowRTL(false);
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Root);
