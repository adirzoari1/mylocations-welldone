import React from 'react';

import { createBottomTabNavigator, createAppContainer, createStackNavigator } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { CategoriesScreen, LocationsScreen, AddCategoryScreen, AddLocationScreen } from '../screens'

import TabStack from './TabStack'
import { Header } from '../components';
let MainStack = createStackNavigator({
  TabStack: TabStack,
  AddCategoryScreen: AddCategoryScreen,
  AddLocationScreen: AddLocationScreen

}, {

    initialRouteName: 'TabStack',
    headerMode:'screen',
    lazy: true,

    defaultNavigationOptions: {
            
      header: props => (<Header navigation = {props.navigation} />),
      gesturesEnabled: false,
    },
  
    
  },


);

export default createAppContainer(MainStack);
