import React, { Component } from 'react'
import {
    View,
    AsyncStorage,
    AppState,
    ImageBackground,
    BackHandler,
    Platform,
    Alert,
    ToastAndroid,
    StatusBar,
    Keyboard

} from 'react-native'

import { observer, Provider } from 'mobx-react/native'
import { create } from 'mobx-persist'

import { CategoriesScreen, LocationsScreen } from '../screens'
import { SplashScreen, Spinner, Header } from '../components'

import { CategoriesStore, LocationsStore, AppStore } from '../stores'
import { calcSize } from '../utils/Consts'
import colors from '../utils/Colors';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import MainStack from './MainStack'

let hydrate = create({
    storage: AsyncStorage
})
const stores = { CategoriesStore, LocationsStore, AppStore }


hydrate('categories', CategoriesStore).then(() => {
    CategoriesStore.doneHydrating();
}).catch((e) => { console.log(e) })


hydrate('locations', LocationsStore).then(() => {
    LocationsStore.doneHydrating();
}).catch((e) => { console.log(e) })





@observer
class Root extends Component {
    constructor(props) {
        super(props)
        this.state = {
            nowMounted: false,
            splashDone: false

        }



    }
    componentWilMount = () => {
        this.setState({ nowMounted: true })

    }
    componentDidMount = () => {
        setTimeout(() => {
            this.setState({ splashDone: true })
        }, 3000);
    }

    getActiveRouteName = (navigationState) => {
        if (!navigationState) {
            return null;
        }
        const route = navigationState.routes[navigationState.index];
        if (route.routes) {
            return this.getActiveRouteName(route);
        }

        return route.routeName;
    }
    getScreenName(navigationState, params) {
        let currentScreen = this.getActiveRouteName(navigationState)
        
        if (params && params.id >= 0) {
            switch (currentScreen) {
                case 'AddLocationScreen': {
                    currentScreen = 'EditLocationScreen';
                    break;
                }
                case 'AddCategoryScreen': {
                    currentScreen = 'EditCategoryScreen';
                    break;
                }
            }
        }
        return currentScreen
    }

    render() {

        const ready = LocationsStore.done && CategoriesStore.done
        const splashDone = LocationsStore.done && CategoriesStore.done && this.state.splashDone;
        return (
            <View style={{ flex: 1, backgroundColor: 'transparent' }} >
                <StatusBar
                    barStyle="dark-content"
                />
                {!splashDone ? <SplashScreen /> : null}
                {
                    ready &&
                    <Provider {...stores}>
                        <View style={{ flex: 1 }}>
                            <MainStack
                                onNavigationStateChange={(oldState, newState, action) => {

                                    if (action.type === 'Navigation/BACK') {
                                        const currentScreen = this.getScreenName(newState, action.params);
                                        AppStore.setRouteName(currentScreen);
                                    } else if (action.type === 'Navigation/NAVIGATE') {
                                        const currentScreen = this.getScreenName(newState, action.params);
                                        AppStore.setRouteName(currentScreen);

                                    } else if (action.type === 'Navigation/RESET') {
                                        const currentScreen = this.getScreenName(newState, action.params);
                                        AppStore.setRouteName(currentScreen);
                                    }


                                }} />
                        </View>




                    </Provider>
                }


                {
                    AppStore.loading ?
                        <Spinner
                            color={colors.loading}
                            style={{ position: 'absolute', right: 0, left: 0, top: 0, bottom: 0, zIndex: 99999 }} />
                        : null
                }

            </View >
        )
    }

}
export default Root