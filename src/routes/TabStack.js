import React from 'react';

import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import Icon from 'react-native-vector-icons/Feather';
import { CategoriesScreen, LocationsScreen } from '../screens'
import { colors } from '../utils/Colors'
let TabStack = createBottomTabNavigator(
    {
        Categories: CategoriesScreen,
        Locations: LocationsScreen,
    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                const { routeName } = navigation.state;
                let iconName;
                if (routeName === 'Categories') {
                    iconName = 'book'
                } else if (routeName === 'Locations') {
                    iconName = 'map-pin'
                }

                return <Icon name={iconName} size={horizontal ? 20 : 25} color={tintColor} />;
            },
        }),
        tabBarOptions: {
            activeTintColor: '#E9EBEF',
            inactiveTintColor: '#8C5CED',
            style: {
                backgroundColor: '#2D324E',//color you want to change
              }
        },
    }
)

export default createAppContainer(TabStack);
