import { observable, action, computed, toJS} from 'mobx'
import { persist } from 'mobx-persist'
// import moment from 'moment'
import _ from 'lodash'

class LocationsStore {

    @observable done = false;
    @persist @observable id_location = 0;
    @persist('list') @observable locations = []

    constructor() {
     this.doneHydrating = this.doneHydrating.bind(this)
     this.add = this.add.bind(this)
     this.update = this.update.bind(this)
     this.remove = this.remove.bind(this)
     this.getLocationById = this.getLocationById.bind(this)

    }

    @action
    doneHydrating() {
        this.done = !this.done;
    }
    
    @action 
    add = (location)=>{
        location.id = this.id_location++;;
        this.locations.push(location)

    }

    @action 
    update =  (item)=>{
        let tempLocations = this.locations.peek()
        let tempIndex = _.findIndex((tempLocations),(l)=>{return l.id === item.id})
        tempLocations[tempIndex] = item;
        this.locations = tempLocations
    }

    @action 
    remove = (id)=>{
        let tempLocations = this.locations.peek()
        _.remove(tempLocations, c => c.id === id);
        
        this.locations = tempLocations;

    }

    @action
    getLocationById = (id)=>{
        let location = _.filter((this.Locations),(l)=>{return l.id === id})[0]

        return location;
    }

    @computed
    get Locations (){
        return toJS(this.locations)
    }

    
 

    

  
    

}

const singelton = new LocationsStore()
export default singelton