import { observable, action, computed, toJS } from 'mobx'
import { persist } from 'mobx-persist'
// import moment from 'moment'
import _ from 'lodash'

class CategoriesStore {

    @observable done = false;
    @persist @observable exa = false;
    @persist @observable id_category = 0;
    @persist('list') @observable categories = []

    constructor() {
        this.add = this.add.bind(this)
        this.update = this.update.bind(this)
        this.remove = this.remove.bind(this)
        this.getCategoryById = this.getCategoryById.bind(this)
        this.doneHydrating = this.doneHydrating.bind(this)
    }

    @action
    add = (name) => {
        this.categories.push({ id: this.id_category++, name })
    }

    @action
    update = (item) => {
        let tempCategories = this.categories.peek()
        let tempIndex = _.findIndex((tempCategories), (c) => { return c.id === item.id })
        tempCategories[tempIndex].name = item.name;
        this.categories = tempCategories
    }

    @action
    remove = (id) => {
        console.log('id',id)
        console.log('categories',this.categories.peek())
        let tempCategories = this.categories.peek()
        _.remove(tempCategories, c => c.id === id);
        this.categories = tempCategories;

    }

    getCategoryById = (id) => {
        let category = _.filter((this.Categories), (c) => { return c.id === id })[0]

        return category;
    }

    @action
    doneHydrating = () => {
        this.done = !this.done;
    }

    @computed
    get Categories() {
        return toJS(this.categories)
    }





}

const singelton = new CategoriesStore()
export default singelton