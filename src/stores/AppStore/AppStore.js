import { observable, action, computed, toJS } from 'mobx'
import { persist } from 'mobx-persist'
import { BackHandler, Alert } from 'react-native'
import { NavigationActions, StackActions } from 'react-navigation';
import { CategoriesStore } from '..'
// import moment from 'moment'
import _ from 'lodash'




class AppStore {

    @observable routeName = 'Categories';
    @observable loading = false

    @observable done = false
    @observable appCurrentRoute = null
    
    @observable locationModalVisible = false;
    @observable locationModalRef = null


    constructor() {

        this.doneHydrating = this.doneHydrating.bind(this)
        this.setRouteName = this.setRouteName.bind(this)
        this.setLoading = this.setLoading.bind(this)
        this.hideLocationModal = this.hideLocationModal.bind(this)
        this.showLocationModal = this.showLocationModal.bind(this)
        this.setLocationModalRef = this.setLocationModalRef.bind(this)
    }

  
    @action
    doneHydrating() {
        this.done = !this.done;

    }

    @action
    setRouteName = (routeName) => {
        this.routeName = routeName
    }


    @action
    setLoading = () => {
        this.loading = !this.loading;
    }

    @action
    showLocationModal = ()=>{
        this.locationModalVisible = true;
    }

    @action
    hideLocationModal = ()=>{
        this.locationModalVisible = false;

    }

    
    @action
    hideLocationModal = ()=>{
        this.locationModalVisible = false;

    }

    @action
    setLocationModalRef = (ref)=>{
        this.locationModalRef = ref;
    }


    @computed 
    get HeaderTitle() {
            switch (this.routeName) {
                case 'Categories':
                    return 'Categories'
                    break
                case 'Locations':
                    return 'Locations'
                    break
                case 'AddLocationScreen':
                    return 'Add Location'
                    break
                case 'EditLocationScreen':
                    return 'Edit Location'
                    break
                case 'AddCategoryScreen':
                    return 'Add Category'
                    break
                case 'EditCategoryScreen':
                    return 'Edit Category'
                    break
                default:
                    return false
                    break
            }
    }





}

const singelton = new AppStore()
export default singelton