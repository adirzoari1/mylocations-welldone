import { StyleSheet, Dimensions } from 'react-native'
import { calcSize } from '../../utils/Consts'
import colors from '../../utils/Colors'
import Fonts from '../../utils/Fonts';


const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.dark_blue,
        position: 'relative',
        minWidth: '100%',
        minHeight: '100%',
        justifyContent:'center',
        alignItems:'center'
    },
    imgLogo: {
        width: calcSize(250),
        height: calcSize(333),
    },
    viewLogo:{
        marginTop:calcSize(20)
    },
    txtLogo:{
        fontSize:calcSize(30),
        color:colors.white,
        fontFamily:Fonts.semi_bold,
    },
    viewWelldone:{
        marginTop:calcSize(30)

    },
    txtWelldone:{
        alignSelf:'center',
        fontSize:calcSize(20),
        color:colors.white,
        fontFamily:Fonts.light,  
    },
    viewTextPowered:{
        position:'absolute',
        bottom:calcSize(20)
    },
    txtPowered:{
        fontSize:calcSize(30),
        color:colors.white,
        fontFamily:Fonts.regular,
    },

})

export default Styles
