import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    ImageBackground
} from 'react-native'

import Styles from './style'
import * as Animatable from 'react-native-animatable';


export default class SplashScreen extends Component {

    render() {
        const { container, imgLogo, viewTextPowered, txtPowered, viewLogo, txtLogo, viewWelldone, txtWelldone} = Styles
        return (
            <View style={container}>
                <Animatable.Image iterationCount={2} duration={1000} animation="flash" resizeMethod='resize' resizeMode='contain' style={imgLogo} source={require('../../assets/images/logo.png')} />
                <View style={viewLogo}>
                    <Text style={txtLogo}>MyLocations</Text>
                </View>
              
                <View style={viewTextPowered}>
                    <Text style={txtWelldone}>Pracitcal Exam For Welldone</Text>

                    <Text style={txtPowered}>POWERED BY ADIR ZOARI</Text>
                   </View>

            </View>
        )
    }
}

