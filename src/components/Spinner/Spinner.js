import React from 'react'
import { Platform, ActivityIndicator } from 'react-native'
import colors from '../../utils/Colors'
import { calcSize } from '../../utils/Consts'

const spinnerSizes = {
    LARGE: Platform.OS === 'ios' ? 'large' : calcSize(68),
    SMALL: Platform.OS === 'ios' ? 'small' : calcSize(30)
}

const Spinner = props => 
    <ActivityIndicator
        style={props.style}
        size={props.size || spinnerSizes.LARGE}
        color={props.color || colors.black} />

export default Spinner

export { spinnerSizes }