import { StyleSheet, Dimensions } from 'react-native'
import { calcSize, DIMENSIONS, EDGE_DEVICE } from '../../utils/Consts'
import colors from '../../utils/Colors'
import Fonts from '../../utils/Fonts';


const Styles = StyleSheet.create({
    viewCategoryItem:{
        backgroundColor:colors.white,
        width:DIMENSIONS.width - calcSize(20),
        height:calcSize(100),
        margin:calcSize(20),
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        padding:calcSize(15)
    },
    viewBarButtons:{
        flexDirection:'row',
       
    },
    buttonIcon:{
        backgroundColor:colors.light_grey,
        width:calcSize(80),
        height:calcSize(80),
        borderRadius:calcSize(40),
        margin:calcSize(10),
        justifyContent:'center',
        alignItems:'center'
    },
    viewTextCategory:{
        width:calcSize(350)

    },
    txtCategory:{
        fontSize:calcSize(25),
        color:colors.purple,
        fontFamily:Fonts.medium,  
    }
})

export default Styles
