import React, { Component } from 'react'
import {
    View,
    Text,
    TouchableHighlight,
    Alert
} from 'react-native'

import Styles from './style'
import Icon from 'react-native-vector-icons/Feather';
import { Button } from '..'
import { calcSize } from '../../utils/Consts'
import colors from '../../utils/Colors'
import { inject, observer } from 'mobx-react/native'
import _ from 'lodash'

@inject('CategoriesStore','LocationsStore', 'AppStore') @observer

class CategoryItem extends Component {

    constructor(props) {
        super(props)
        this.state = {
            activeEdit: false,
            activeDelete: false,
            id: this.props.id
        }
    }

    navigateToEditCategory = () => {
        this.props.navigation.navigate('AddCategoryScreen', { id: this.props.id })
    }

    componentWillUnmount = () => {
        this.setState({ activeEdit: false, activeDelete: false })
    }
    removeCategory = () => {
        const { LocationsStore } = this.props
        let checkIfCategoryAssignedToLocation = LocationsStore.Locations && LocationsStore.Locations.filter((l) => { return _.includes(l.categories, this.state.id) })
        console.log('checkIf',checkIfCategoryAssignedToLocation)
        if (checkIfCategoryAssignedToLocation &&checkIfCategoryAssignedToLocation.length>0 ) {
            Alert.alert('This category assigned to location. You cannot remove')
        }
        else {
                Alert.alert(
                    'Remove Category',
                    'Are you sure you want to delete?',
                    [
                        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                        {
                            text: 'OK', onPress: () => {
                                this.props.CategoriesStore.remove(this.state.id)
                            }
                        },
                    ],
                    { cancelable: false }
                )
            }


        }

        render() {
            const { id, name } = this.props
            const { activeEdit, activeDelete } = this.state
            const { viewCategoryItem, viewBarButtons, buttonIcon, viewTextCategory, txtCategory } = Styles
            const activeEditBtn = activeEdit ? { backgroundColor: colors.purple } : { backgroundColor: colors.light_grey }
            const activeDeleteBtn = activeDelete ? { backgroundColor: colors.purple } : { backgroundColor: colors.light_grey }

            return (

                <View style={viewCategoryItem}>
                    <View style={viewTextCategory}>
                        <Text numberOfLines={1} style={txtCategory}>{name}</Text>
                    </View>
                    <View>
                    </View>
                    <View style={viewBarButtons}>
                        <Button style={[buttonIcon, activeEditBtn]} onPress={() => { this.navigateToEditCategory() }}>
                            <Icon name='edit' size={calcSize(30)} color={activeEdit ? colors.white : colors.dark_blue} />
                        </Button>
                        <Button style={[buttonIcon, activeDeleteBtn]} onPress={() => { this.removeCategory() }}>
                            <Icon name='trash-2' size={calcSize(30)} color={activeDelete ? colors.white : colors.dark_blue} />
                        </Button>
                    </View>

                </View>
            )
        }
    }

    export default CategoryItem