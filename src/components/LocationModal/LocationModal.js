import React, { Component } from 'react'
import {
    Text,
    View,
    Animated,
    Image,
    ImageBackground
} from 'react-native'

import { inject, observer } from 'mobx-react/native'
import Icon from 'react-native-vector-icons/Feather';
import { NavigationActions } from 'react-navigation'
import { Button } from '..'
import Styles from './style'
import { calcSize } from '../../utils/Consts';
import colors from '../../utils/Colors';
import Modal from "react-native-modal";
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import _ from 'lodash'

@inject('AppStore', 'CategoriesStore') @observer
export default class LocationModal extends Component {

    constructor(props) {
        super(props)
        this.state = {
            modalVisible: false,
            popupType: '',
            coordinates: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0,
                longitudeDelta: 0

            },
        }

        this.setModalInvisible = this.setModalInvisible.bind(this)
        this.setModalVisible = this.setModalVisible.bind(this)
        this.onModalHide = this.onModalHide.bind(this)
        this.renderModalByType = this.renderModalByType.bind(this)
    }

    componentDidMount() {
        try {
            this.props.onRef(this)

        } catch (e) {
            console.log(e)
        }
    }
    componentWillUnmount() {
        this.props.onRef(undefined)
    }

    setModalVisible = async (info, popupType, errorObject = {}) => {
        let coordinates = {
            ...info.coordinates,
            latitudeDelta: Math.abs(info.coordinates.latitude / 5000),
            longitudeDelta: Math.abs(info.coordinates.longitude / 5000),
        }
        let categoriesArray = _.filter(this.props.CategoriesStore.Categories, (c) => _.indexOf(info.categories, c.id) > -1)
        let new_info = { ...info, coordinates, categories: categoriesArray }


        await this.setState({ ...new_info, popupType, errorObject })
        this.props.AppStore.showLocationModal()
    }
    setModalInvisible = async () => {

        this.props.AppStore.hideLocationModal()


    }
    onModalHide = () => {

    }
    renderDetailsLocation = () => {
        const { name, address, coordinates, categories } = this.state
        const { viewDetailsLocation, viewMapIcon, viewDetails, viewNameLocation, txtHeaderLocation, txtLocationValue, viewDetailsLocationSection, viewDetailsCoordinates, viewHeaderCoordinates, txtCoordinatesHeader, viewDetailsCoordinatesInner, viewCategories, txtCoordinateHeader, viewCategoryItem, txtCategoryItem } = Styles;
        return (
            <View >
            <View style={viewMapIcon}>
                    <Icon name='map-pin' size={calcSize(60)} color={colors.purple} />
                </View>
                <View style={viewDetailsLocation}>

                <View style={viewDetails}>
                    <View style={viewDetailsLocationSection}>
                        <Text style={txtHeaderLocation}>Name</Text>
                        <Text style={txtLocationValue}>{this.state.name}</Text>
                    </View>
                    <View style={viewDetailsLocationSection}>
                        <Text style={txtHeaderLocation}>Address</Text>
                        <Text style={txtLocationValue}>{this.state.address}</Text>
                    </View>
                    <View style={viewDetailsLocationSection}>
                        <View style={viewHeaderCoordinates}>
                            <Text style={txtHeaderLocation}>Coordinates</Text>
                        </View>
                        <View style={viewDetailsCoordinates}>
                            <View style={viewDetailsCoordinatesInner}>
                                <Text style={txtCoordinateHeader}>Latitude</Text>
                                <Text style={txtLocationValue}>{this.state.coordinates.latitude}</Text>
                            </View>
                            <View style={viewDetailsCoordinatesInner}>
                                <Text style={txtCoordinateHeader}>Longitue</Text>
                                <Text style={txtLocationValue}>{this.state.coordinates.longitude}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={viewDetailsLocationSection}>
                        <Text style={txtHeaderLocation}>Categories</Text>
                        <View style={viewCategories}>
                            {this.state.categories && this.state.categories.map((c, i) => (
                                <View key={i} style={viewCategoryItem}>
                                    <Text style={txtCategoryItem}>{c.name}</Text>
                                </View>
                            ))}
                        </View>
                    </View>

                </View>
                </View>
            </View>

        )
    }
    renderMap = () => {
        const { viewMap, map, viewDetailsMap, txtDetailMap } = Styles
        return (
            <View style={viewMap}>
                <MapView style={map}
                    region={this.state.coordinates}
                    onUserLocationChange={(e) => { 'onUserLocation', console.log(e.nativeEvent) }}
                    // onPress={(e) => { this.setState({  longitude: e.nativeEvent.coordinate.longitude, latitude: e.nativeEvent.coordinate.latitude }) }}
                    onPress={() => { }}
                    onCalloutPress={(e) => { 'onCalloutPress', console.log(e.nativeEvent) }}


                >
                    {this.state.coordinates.latitude != 0 && this.state.coordinates.longitude != 0 && <MapView.Marker
                        coordinate={{ "latitude": this.state.coordinates.latitude, "longitude": this.state.coordinates.longitude }}
                        title={"Your Location"}
                    />}
                </MapView>
                <View style={viewDetailsMap}>
                    <Text style={txtDetailMap}>latitude: {this.state.coordinates.latitude}</Text>
                    <Text style={txtDetailMap}>longitude: {this.state.coordinates.longitude}</Text>
                </View>
            </View>
        )
    }



    renderModalByType(popupType) {
        switch (popupType) {
            case 'map': {
                return this.renderMap()
            }
            case 'details_location': {
                return this.renderDetailsLocation()
            }
            default:
                break;

        }
    }

    render() {

        let { valueC, date, service_type, time, popupType, modalVisible } = this.state
        const { AppStore } = this.props

        const { container, btnIconClose, iconClose, viewIconClose, viewModalContainer } = Styles;
        return (

            <Modal
                isVisible={AppStore.locationModalVisible}
                backdropColor={colors.transparent_b}
                backdropOpacity={1}
                animationIn="bounceInDown"
                hideModalContentWhileAnimating={true}
                animationInTiming={1000}
                animationOutTiming={600}
                useNativeDriver={true}
                onCancelPressed={() => this.setModalInvisible()}
                onBackButtonPressed={() => this.setModalInvisible()}
                onModalHide={() => { this.setModalInvisible() }}
                onRequestClose={() => { this.setModalInvisible() }}

            >
                <View style={container}>
                    <Button style={btnIconClose} onPress={() => { this.setModalInvisible() }} >
                        <Icon name="x" size={calcSize(45)} color={colors.yellow} style={iconClose} />
                    </Button>
                    <View style={viewModalContainer}>

                        {this.renderModalByType(popupType)}

                    </View>
                </View>
            </Modal >
        )
    }
}
