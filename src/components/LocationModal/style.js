import { StyleSheet, Dimensions } from 'react-native'
import { calcSize, DIMENSIONS, EDGE_DEVICE } from '../../utils/Consts'
import colors from '../../utils/Colors';
import Fonts from '../../utils/Fonts';
const screenHeight = Dimensions.get('window').height
const { width } = Dimensions.get('window')
const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'transparent',
        justifyContent: 'center',
        alignItems: 'center',
        shadowOpacity: 0,
    },
    viewModalContainer: {
        width: DIMENSIONS.width - calcSize(100),
        height:  DIMENSIONS.height - calcSize(200),
        borderRadius: calcSize(30),
        backgroundColor: colors.red,
        alignItems: 'center',
        justifyContent:'center',


    },

    viewIconClose:{
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: calcSize(10),
        paddingRight: calcSize(10),
        position: 'absolute',
        top: calcSize(3),
        right: calcSize(-20),
        zIndex: 999,
        shadowOpacity:0
    },
    btnIconClose:{
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: calcSize(10),
        paddingRight: calcSize(10),
        position: 'absolute',
        top: calcSize(-10),
        right: calcSize(-10) ,
        zIndex: 999,
        shadowOpacity:0

    },
    iconClose:{
        alignSelf: 'center',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',

    },
    map: {
        ...StyleSheet.absoluteFillObject,
      },
    viewMap: {
        width: DIMENSIONS.width - calcSize(100),
        height:  DIMENSIONS.height - calcSize(350),
        marginTop:calcSize(20),
    },
    viewDetailsMap:{
        backgroundColor:colors.transparent_a,
        height:calcSize(150),
        minWidth:calcSize(300),
        justifyContent:'center',
        alignItems:'center',
        borderRadius:calcSize(30),
        position:'absolute',
        bottom:0,
        right:0
    },
    txtDetailMap:{
        fontFamily:Fonts.medium,
        fontSize: calcSize(20),
        color:colors.purple
    },
    viewDetailsLocation:{
        width: DIMENSIONS.width - calcSize(100),
        minHeight:  DIMENSIONS.height - calcSize(500),
        borderRadius: calcSize(30),
        backgroundColor: colors.white,
        alignItems:'center',
        justifyContent:'center',
        borderColor:colors.purple,
        borderWidth:calcSize(4),
      
    },
    viewMapIcon:{
        zIndex:9999,
        alignSelf:'center',
        backgroundColor:colors.yellow,
        width:calcSize(100),
        height:calcSize(100),
        borderRadius:calcSize(50),
        justifyContent:'center',
        alignItems:'center',
        position:'absolute',
        top:calcSize(-40),
        borderColor:colors.purple,
        borderWidth:calcSize(4),
    },
    viewDetails:{

    },
    viewDetailsLocationSection:{
        margin:calcSize(20)
    },
    txtHeaderLocation:{
        fontFamily:Fonts.semi_bold,
        fontSize: calcSize(25),
        color:colors.purple,
        marginRight:calcSize(20)
    },
    txtLocationValue:{
        fontFamily:Fonts.light,
        fontSize: calcSize(25),
        color:colors.dark_blue
    },
    viewDetailsCoordinates:{
        flexDirection:'row',
        marginLeft:calcSize(20),
    },
    txtCoordinateHeader:{
        fontFamily:Fonts.medium,
        fontSize: calcSize(23),
        color:colors.green,
        marginBottom:calcSize(20)
    },
    viewCategories:{
        flexDirection:'row',
        flexWrap:'wrap',
    },
    viewCategoryItem:{
        minWidth:calcSize(180),
        height:calcSize(60),
        backgroundColor:colors.yellow,
        borderColor:colors.purple,
        borderWidth:calcSize(3),
        borderRadius:calcSize(20),
        justifyContent:'center',
        alignItems:'center',
        margin:calcSize(10),

    },
    viewDetailsCoordinatesInner:{
        margin:calcSize(10),
    },
    txtCategoryItem:{
        fontFamily:Fonts.medium,
        fontSize: calcSize(23),
        color:colors.purple,
    }
  
})
export default Styles
