import React from 'react'
import {
    Platform,
    View,
    TouchableOpacity,
    TouchableNativeFeedback
} from 'react-native'

const _handlePress = callback => requestAnimationFrame(callback)


const Button = (props) => {
    return (
        Platform.OS === 'ios'
            ? <TouchableOpacity disabled={props.disabled} style={props.style} onPress={props.onPress}>{props.children}</TouchableOpacity> 
            : <TouchableNativeFeedback  Ripple = {props.ripple} disabled={props.disabled} onPress={props.onPress}><View style={props.style}>{props.children}</View></TouchableNativeFeedback>
    )
}

Button.defaultProps = {
    onPress : () => {}
}

export default Button