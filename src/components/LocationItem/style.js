import { StyleSheet, Dimensions } from 'react-native'
import { calcSize, DIMENSIONS, EDGE_DEVICE } from '../../utils/Consts'
import colors from '../../utils/Colors'
import Fonts from '../../utils/Fonts';


const Styles = StyleSheet.create({
    viewCategoryItem:{
        justifyContent:'space-around',
        backgroundColor:colors.white,
        width:DIMENSIONS.width - calcSize(100),
        height:calcSize(180),
        borderRadius:calcSize(30),
        margin:calcSize(40),
        padding:calcSize(10)
    },
    viewMapIcon:{
        position:'absolute',
        zIndex:1,
        top:calcSize(5),
        right:calcSize(30),
        backgroundColor:colors.yellow,
        width:calcSize(100),
        height:calcSize(100),
        borderRadius:calcSize(50),
        justifyContent:'center',
        alignItems:'center',
    },
    viewBarButtons:{
        flexDirection:'row',
        justifyContent:'center'
       
    },
    buttonIcon:{
        backgroundColor:colors.light_grey,
        width:calcSize(80),
        height:calcSize(80),
        borderRadius:calcSize(40),
        margin:calcSize(20),
        justifyContent:'center',
        alignItems:'center'
    },
    viewTextCategory:{
        width:calcSize(350),
        marginLeft:calcSize(20)

    },
    txtCategory:{
        fontSize:calcSize(30),
        color:colors.purple,
        fontFamily:Fonts.medium,  
    }
})

export default Styles
