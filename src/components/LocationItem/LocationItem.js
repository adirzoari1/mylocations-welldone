import React, { Component } from 'react'
import {
    View,
    Text,
    TouchableHighlight,
    Alert,
    Vibration
} from 'react-native'

import Styles from './style'
import Icon from 'react-native-vector-icons/Feather';
import { Button } from '..'
import { calcSize } from '../../utils/Consts'
import colors from '../../utils/Colors'
import { inject, observer } from 'mobx-react/native'


@inject('LocationsStore', 'AppStore') @observer

class LocationItem extends Component {

    constructor(props) {
        super(props)
        this.state = {
            activeEdit: false,
            activeDelete: false,
            id: this.props.id
        }
        this.navigateToEditLocation = this.navigateToEditLocation.bind(this)
        this.removeLocation = this.removeLocation.bind(this)
        this.viewDetailsLocation = this.viewDetailsLocation.bind(this)
        this.viewOnMap = this.viewOnMap.bind(this)
    }

   

    componentWillUnmount = () => {
        this.setState({ activeEdit: false, activeDelete: false })
    }
    navigateToEditLocation = () => {
        this.props.navigation.navigate('AddLocationScreen', { id: this.props.id })
    }
    removeLocation = () => {
        Alert.alert(
            'Remove Location',
            'Are you sure you want to delete?',
            [
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                {
                    text: 'OK', onPress: () => {
                        // this.props.LocationsStore.remove(this.state.id)
                        this.props.removeItem(this.state.id)
                    }
                },
            ],
            { cancelable: false }
        )
    }

    viewOnMap = (data)=>{
        Vibration.vibrate(1000)

        this.props.AppStore.locationModalRef && this.props.AppStore.locationModalRef.setModalVisible(data, 'map')

    }

    viewDetailsLocation = (data)=>{
        Vibration.vibrate(1000)

        this.props.AppStore.locationModalRef && this.props.AppStore.locationModalRef.setModalVisible(data, 'details_location')

    }

    render() {
        const { id, name } = this.props
        const { activeEdit, activeDelete } = this.state
        const { viewCategoryItem, viewBarButtons, viewMapIcon, buttonIcon, viewTextCategory, txtCategory } = Styles
        const activeEditBtn = activeEdit ? { backgroundColor: colors.purple } : { backgroundColor: colors.light_grey }
        const activeDeleteBtn = activeDelete ? { backgroundColor: colors.purple } : { backgroundColor: colors.light_grey }

        return (

            <View style={{}}>
                <View style={viewMapIcon}>
                    <Icon name='map-pin' size={calcSize(60)} color={colors.purple} />

                </View>
                <View style={viewCategoryItem}>
                    <View style={viewTextCategory}>
                        <Text numberOfLines={1} style={txtCategory}>{name}</Text>
                    </View>
                    <View></View>

                    <View style={viewBarButtons}>
                        <Button style={buttonIcon} onPress={() => { this.viewOnMap(this.props)}}>
                            <Icon name='map' size={calcSize(30)} color={colors.dark_blue} />
                        </Button>
                        <Button style={buttonIcon} onPress={() => { this.viewDetailsLocation(this.props) }}>
                            <Icon name='book' size={calcSize(30)} color={colors.dark_blue} />
                        </Button>
                        <Button style={buttonIcon} onPress={() => { this.navigateToEditLocation() }}>
                            <Icon name='edit' size={calcSize(30)} color={colors.dark_blue} />
                        </Button>
                        <Button style={buttonIcon} onPress={() => { this.removeLocation() }}>
                            <Icon name='trash-2' size={calcSize(30)} color={colors.dark_blue} />
                        </Button>
                    </View>
                </View>

            </View>
        )
    }
}

export default LocationItem