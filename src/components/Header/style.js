import { StyleSheet, Dimensions } from 'react-native'
import { calcSize, DIMENSIONS, EDGE_DEVICE } from '../../utils/Consts'
import colors from '../../utils/Colors'
import Fonts from '../../utils/Fonts';


const Styles = StyleSheet.create({
    viewHeader:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        backgroundColor:colors.dark_blue,
        height:calcSize(100)
    },
    viewLeftHeader:{
        marginLeft:calcSize(20)
    },
    viewTextHeader:{
        justifyContent:'center',
        alignItems:'center'
    },
    txtHeader:{
        fontSize:calcSize(28),
        color:colors.white,
        fontFamily:Fonts.medium, 
    },
    imgLogo:{
        width:calcSize(60),
        height:calcSize(60)
    },
    viewRightSide:{
        marginRight:calcSize(10)
    },
    viewIcon:{
        backgroundColor:colors.dark_blue_a,
        justifyContent:'center',
        alignItems:'center',
        width:calcSize(80),
        height:calcSize(80),
        borderRadius:calcSize(40)
    }
})

export default Styles
