import React, { Component } from 'react'
import {
    View,
    Text,
    TouchableHighlight,
    Alert
} from 'react-native'

import Styles from './style'
import Icon from 'react-native-vector-icons/Feather';
import { Button } from '..'
import { calcSize } from '../../utils/Consts'
import colors from '../../utils/Colors'
import { inject, observer } from 'mobx-react/native'
import * as Animatable from 'react-native-animatable';

let headersGoBack = ['AddLocationScreen', 'EditLocationScreen', 'AddCategoryScreen', 'EditCategoryScreen']

@inject('CategoriesStore', 'LocationsStore', 'AppStore') @observer

class Header extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    handleAddButton = () => {
        const { AppStore,CategoriesStore } = this.props
        switch (AppStore.routeName) {
            case 'Categories': {
                this.props.navigation.navigate('AddCategoryScreen')
                break;
            }
            case 'Locations': {
                if (CategoriesStore.Categories.length > 0) {
                    this.props.navigation.navigate('AddLocationScreen')
                    break;

                } else {
                    Alert.alert('Please add categories First')
                    break;
                }

            }
        }

    }
    render() {
        const { AppStore, CategoriesStore, LocationsStore } = this.props
        const { viewHeader, viewLeftHeader, viewTextHeader, viewRightSide, txtHeader, imgLogo, viewIcon } = Styles;
        let canGoBack = headersGoBack.includes(AppStore.routeName)
        
        return (

            <View style={viewHeader}>
                <View style={viewLeftHeader}>
                    {canGoBack ?
                        <Button onPress={() => { this.props.navigation.goBack(null) }} style={viewIcon}>

                            <Icon name='arrow-left' size={calcSize(40)} color={colors.white} />
                        </Button>
                        :
                        <Animatable.Image iterationCount="infinite" iterationDelay={2000} duration={1000} animation="flash" resizeMethod='resize' resizeMode='contain' style={imgLogo} source={require('../../assets/images/logo.png')} />
                    }

                </View>
                <View style={viewTextHeader}>
                    <Text style={txtHeader}>{AppStore.HeaderTitle}</Text>
                </View>
                <View style={viewRightSide}>
                    {!canGoBack && <Button onPress={() => { this.handleAddButton() }} style={viewIcon}>

                        <Icon name='plus' size={calcSize(40)} color={colors.white} />
                    </Button>
                    }
                </View>


            </View>
        )
    }
}

export default Header