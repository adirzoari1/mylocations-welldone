import validator from "validatorjs";
import en from 'validatorjs/src/lang/en';
validator.setMessages('en',en)

const customValidator = (data, rules) => {
    return new validator(data, rules);
  };
const locationForm = {
    name: 'required',
    address:'required',
    latitude:'required',
    longitude:'required',
    categories:'required|array|min:1'
}

const categoriesForm = {
    name:'required'
}
let messages = validator.getMessages('en');


export {locationForm, categoriesForm,customValidator}