export default {
    light: 'Poppins-Light',
    medium: 'Poppins-Medium',
    regular: 'Poppins-Regular',
    bold: 'Poppins-Bold',
    semi_bold: 'Poppins-SemiBold',
}