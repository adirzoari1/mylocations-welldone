import {Dimensions } from "react-native"

const { height, width } = Dimensions.get('window')


export const DIMENSIONS = { height, width }
const DESIGN_WIDTH = 750
const PIXEL_RATIO = DESIGN_WIDTH / width

export const calcSize = size => size / PIXEL_RATIO



