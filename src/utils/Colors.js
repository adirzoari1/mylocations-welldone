
const colors = {
    transparent: 'rgba(0,0,0,0)',
    transparent_a: 'rgba(255,255,255,0.7)',
    transparent_b: 'rgba(0,0,0,0.7)',


    white: '#FFF',
    grey:'#E9EBEF',
    light_grey:'#f0f1f4',
    black: '#000',

    blue:'#3598FE',
    light_blue:'#3BCDE2',

    green:'#04dead',

    dark_blue:'#2D324E',
    dark_blue_a:'#242b46',

    purple:'#8C5CED',
    purple_a:'#5773FF',
    
    pink:'#FF5F99',

    orange:'#FF9057',

    yellow:'#fff200'
   

}

export default colors