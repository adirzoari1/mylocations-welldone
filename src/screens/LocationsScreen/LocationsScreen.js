import React, { Component } from 'react'
import {
    View,
    Text,
    ScrollView,
    FlatList,
    Alert
} from 'react-native'

import { inject, observer } from 'mobx-react/native'
import { observable } from "mobx";

import Icon from 'react-native-vector-icons/Feather'
// import moment from 'moment';

import Styles from './style'
import _ from 'lodash'
import { calcSize } from '../../utils/Consts';
import { LocationItem, LocationModal, Button } from '../../components';
import colors from '../../utils/Colors';
// import * as Animatable from 'react-native-animatable';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';



let SORT_ARRAYS = [
    { name: 'alphabetical', id: 0 },
    // { name: 'grouped', id: 1 },
    // { name: 'ungrouped', id: 2 }

]
@inject('LocationsStore', 'CategoriesStore', 'AppStore') @observer
class LocationsScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            sortBy: [],
            activeCateogryIndex: 0,
            activeCategoryId: 0,
            locations: []

        }

        this.getLocationsByCategoryId = this.getLocationsByCategoryId.bind(this)
        this.navigateTo = this.navigateTo.bind(this)
        this.renderItem = this.renderItem.bind(this)
        this.renderEmptyComponent = this.renderEmptyComponent.bind(this)
        this.onSelectedItemsChange = this.onSelectedItemsChange.bind(this)
        this.sortBy = this.sortBy.bind(this)
        this.chooseCategory = this.chooseCategory.bind(this)

    }

    componentDidMount = () => {
        const { CategoriesStore } = this.props
     
        if (CategoriesStore && CategoriesStore.categories.length>0) {
            let activeCategoryId = CategoriesStore.Categories[0].id
            this.setState({ activeCategoryId })
            this.getLocationsByCategoryId(activeCategoryId)
        }
    }

    getLocationsByCategoryId = (activeCategoryId)=>{
        const {LocationsStore} = this.props
        let locations = LocationsStore.Locations && LocationsStore.Locations.filter((l) => { return _.includes(l.categories, activeCategoryId) })
        this.setState({locations})
    }



    navigateTo = () => {
        const { CategoriesStore } = this.props
        if (CategoriesStore.Categories.length > 0) {
            this.props.navigation.navigate('AddLocationScreen')

        } else {
            Alert.alert('You Should add categories first')
        }
    }

    handleRemoveItem = (id)=>{
        const { LocationsStore } = this.props
        this.props.LocationsStore.remove(id)
        this.getLocationsByCategoryId(this.state.activeCategoryId)


    }
    renderItem = (location, index) => {
        return (
            <LocationItem {...location.item} navigation={this.props.navigation} removeItem ={this.handleRemoveItem.bind(this)} />
        )
    }

    renderEmptyComponent = () => {
        const { viewEmptyLocations, txtEmptyLocations } = Styles;
        return (
            <View style={viewEmptyLocations}>

                <Icon name='map-pin' size={calcSize(100)} color={colors.dark_blue} />
                <Text style={txtEmptyLocations}>No Locations</Text>
            </View>
        )
    }

    onSelectedItemsChange = (sortBy) => {
        this.setState({ sortBy });
        this.sortBy(sortBy[0])

    }
    
    sortBy(id){
        let tempLocations = this.state.locations
        switch(id){
            case 0:{ // alphabetical
                tempLocations = _.orderBy(tempLocations, [location => location.name.toLowerCase()]);
                console.log('tempLocations',tempLocations)
                this.setState({locations:tempLocations})
                break;
            }
            default:{
                break;
            }
        }
    }





    chooseCategory = (i, id) => {
        this.getLocationsByCategoryId(id)
        this.setState({ activeCateogryIndex: i, activeCategoryId: id })
    }

    render() { 
        const { LocationsStore, CategoriesStore } = this.props
        const { container, viewFlatList, txtSortBy, viewContentContainerFlatList,viewButtons, contentFlatList, viewSectionList, activeTxtCategoryItem, chipContainerStyle, chipTextStyle, viewCategories, viewNoActiveCategoryItem, viewActiveCategoryItem, viewCategoryItem, txtCategoryItem } = Styles
        const { activeCateogryIndex, activeCategoryId,locations } = this.state
        let showButtonsHeader = (LocationsStore.locations && LocationsStore.locations.length > 0 && CategoriesStore.categories && CategoriesStore.categories.length>0) 
        return (
            <View style={container}>
                <LocationModal onRef={ref => this.props.AppStore.setLocationModalRef(ref)} />
                {showButtonsHeader &&
                    <View style={viewButtons}>
                        <View style={viewSectionList}>
                            <SectionedMultiSelect
                                single={true}
                                items={SORT_ARRAYS}
                                uniqueKey='id'
                                selectText='SORT BY'
                                showDropDowns={true}
                                onSelectedItemsChange={this.onSelectedItemsChange}
                                hideSearch={true}
                                selectedItems={this.state.sortBy}
                                styles={{
                                    itemBackground: {
                                        backgroundColor: colors.purple
                                    },
                                    button: {
                                        backgroundColor: colors.yellow
                                    },
                                    confirmText: {
                                        color: colors.purple
                                    },
                                    chipContainer: chipContainerStyle,
                                    chipText: chipTextStyle,
                                    selectToggleText: chipTextStyle,
                                    searchPlaceholderTextColor: colors.dark_blue
                                }}
                            />
                        </View>

                        <View style={viewCategories}>
                            {CategoriesStore.Categories && CategoriesStore.Categories.map((c, i) => (
                                <Button key={i} style={[viewCategoryItem, activeCateogryIndex === i ? viewActiveCategoryItem : viewNoActiveCategoryItem]} onPress={() => { this.chooseCategory(i, c.id) }}>
                                    <Text style={activeCateogryIndex == i ? activeTxtCategoryItem : txtCategoryItem}>{c.name}</Text>
                                </Button>
                            ))}
                        </View>
                    </View>
                }
                <ScrollView>
                    <FlatList
                        contentContainerStyle={LocationsStore.locations.length == 0 && viewContentContainerFlatList}
                        data={locations}
                        keyExtractor={(item, index) => `key-${index}`}
                        renderItem={this.renderItem}
                        ListEmptyComponent={this.renderEmptyComponent}
                        showsVerticalScrollIndicator={false}
                        style={contentFlatList}
                    />
                </ScrollView>
            </View>
        )
    }
}

export default LocationsScreen