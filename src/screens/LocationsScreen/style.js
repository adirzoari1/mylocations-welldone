import {StyleSheet,Dimensions} from 'react-native'
import { calcSize, DIMENSIONS, EDGE_DEVICE } from '../../utils/Consts'
import colors from '../../utils/Colors'
import Fonts from '../../utils/Fonts';

const Styles = StyleSheet.create({
 
    container:{
        flex:1,
        backgroundColor:colors.grey,
        justifyContent:'center',
        alignItems:'center',
    },
    viewFlatList:{
        
    },
    contentFlatList:{
   
        
    },
    viewEmptyLocations:{
        marginTop:DIMENSIONS.height/3,
        justifyContent:'center',
        alignItems:'center',

    },
    viewContentContainerFlatList:{
        justifyContent: 'center',
         alignItems: 'center',
        height: '100%' 
    },
    txtEmptyLocations:{
        fontSize:calcSize(28),
        color:colors.dark_blue,
        fontFamily:Fonts.light, 
    },
    chipTextStyle:{
        fontFamily: Fonts.medium,
        fontSize: calcSize(24),
        color:colors.purple
      },
      chipContainerStyle:{
        backgroundColor:colors.yellow,
        borderColor:colors.purple,
        borderWidth:calcSize(3)
      },
      viewSectionList:{
          marginTop:calcSize(10),
          minHeight:calcSize(100),
          width:DIMENSIONS.width- calcSize(150),
       
      },
      txtSortBy:{
        fontFamily: Fonts.medium,
        fontSize: calcSize(24),
        color:colors.purple  
      },
      viewCategories:{
        flexDirection:'row',
        flexWrap:'wrap',
    },
    viewButtons:{
        marginBottom:calcSize(20),
        justifyContent:'center',
        alignItems:'center'
    },
    viewCategoryItem:{
        minWidth:calcSize(190),
        height:calcSize(60),
        padding:calcSize(5),
        borderWidth:calcSize(3),
        borderRadius:calcSize(20),
        justifyContent:'center',
        alignItems:'center',
        margin:calcSize(10),

    },
    viewActiveCategoryItem:{
        backgroundColor:colors.yellow,
        borderColor:colors.purple,
    },
    viewNoActiveCategoryItem:{
        backgroundColor:colors.white,
        borderColor:colors.dark_blue,
    },

    viewDetailsCoordinatesInner:{
        margin:calcSize(10),
    },
    txtCategoryItem:{
        fontFamily:Fonts.medium,
        fontSize: calcSize(23),
        color:colors.dark_blue
    },
    activeTxtCategoryItem:{
        fontFamily:Fonts.bold,
        fontSize: calcSize(26),
        color:colors.purple

    }







    

})

export default Styles
