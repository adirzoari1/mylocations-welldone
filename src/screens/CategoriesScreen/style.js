import {StyleSheet,Dimensions} from 'react-native'
import { calcSize, DIMENSIONS, EDGE_DEVICE } from '../../utils/Consts'
import colors from '../../utils/Colors'
import Fonts from '../../utils/Fonts';

const Styles = StyleSheet.create({
 
    container:{
        flex:1,
        backgroundColor:colors.grey,
        justifyContent:'center',
        alignItems:'center',
    },
    viewFlatList:{
        
    },
    contentFlatList:{
    //   justifyContent:'center',
    //   alignItems:'center'
        
    },
    viewEmptyCategories:{
        marginTop:DIMENSIONS.height/3,
        justifyContent:'center',
        alignItems:'center',
    
    },
    viewContentContainerFlatList:{
        justifyContent: 'center',
         alignItems: 'center',
        height: '100%' 
    },
    txtEmptyCategories:{
        fontSize:calcSize(28),
        color:colors.dark_blue,
        fontFamily:Fonts.light, 
    }







    

})

export default Styles
