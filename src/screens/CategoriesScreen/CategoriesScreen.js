import React, { Component } from 'react'
import {
    View,
    Text,
    ImageBackground,
    Image,
    AppState,
    ScrollView,
    Linking,
    Dimensions,
    FlatList,
} from 'react-native'

import { inject, observer } from 'mobx-react/native'
import Icon from 'react-native-vector-icons/Feather';
// import moment from 'moment';

import Styles from './style'
import _ from 'lodash'
import {Button,CategoryItem} from '../../components';
import { calcSize } from '../../utils/Consts';
import colors from '../../utils/Colors'



@inject('CategoriesStore') @observer
class CategoriesScreen extends Component {
  
    constructor(props) {
        super(props)
        this.renderItem = this.renderItem.bind(this)
        this.renderEmptyComponent = this.renderEmptyComponent.bind(this)
    }

  

    renderItem = (category,index)=>{
        return (
            <CategoryItem {...category.item} navigation ={this.props.navigation}/>
        )
    }
    renderEmptyComponent = ()=>{
        const { viewEmptyCategories, txtEmptyCategories } = Styles;
        return (
            <View style={viewEmptyCategories}>
               
                <Icon name='archive' size={calcSize(100)} color={colors.dark_blue} />
                <Text style={txtEmptyCategories}>No Categories</Text>
            </View>
        )
    }


    render() {

        const { container,viewFlatList,viewContentContainerFlatList,contentFlatList } = Styles
        const { CategoriesStore } = this.props
        return (
            <View style={container}>
               <View style={viewFlatList}>
               <ScrollView>
                <FlatList
                    contentContainerStyle={ viewContentContainerFlatList}
                    data={CategoriesStore.Categories}
                    keyExtractor={(item, index) => `key-${index}`}
                    renderItem={this.renderItem}
                    ListEmptyComponent = {this.renderEmptyComponent}
                    showsVerticalScrollIndicator={false}
                    style={contentFlatList}
                />
                </ScrollView>
                </View>
            </View>

        )
    }
}

export default CategoriesScreen