import React, { Component } from 'react'
import {
    View,
    Text,
    TextInput,
    ScrollView,
    PermissionsAndroid
} from 'react-native'

import { inject, observer } from 'mobx-react/native'
import Icon from 'react-native-vector-icons/Feather'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'; 
import Geocoder from 'react-native-geocoder';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import Validator from 'validatorjs'
import { StackActions, NavigationActions } from 'react-navigation';

import Styles from './style'
import _ from 'lodash'
import Button from '../../components/Button/Button';
import { calcSize } from '../../utils/Consts';
import { locationForm, customValidator } from '../../utils/Validator';
import colors from '../../utils/Colors';



@inject('LocationsStore', 'CategoriesStore', 'AppStore') @observer
class AddLocationScreen extends Component {


    constructor(props) {
        super(props)
        this.state = {
            id: '',
            name: '',
            address: '',
            coordinates: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0,
                longitudeDelta: 0

            },
            categories: [],
            editMode: false,
            valid: {},


        }
        watchId: ''


    }


    componentDidMount = async () => {
        if (this.props.navigation.state.params && this.props.navigation.state.params.id >= 0) {
            await this.setState({ id: this.props.navigation.state.params.id, editMode: true })
            await this.getLocationById(this.props.navigation.state.params.id)

        }
        await this.getLocationPermission()





    }
    componentWillUnmount = () => {
        navigator.geolocation.clearWatch(this.watchID);
    }

    getLocationById = async (id) => {
        const { LocationsStore } = this.props
        let location = LocationsStore.getLocationById(id);
        let coordinates = {
            ...location.coordinates,
            latitudeDelta: Math.abs(location.coordinates.latitude / 5000),
            longitudeDelta: Math.abs(location.coordinates.longitude / 5000),
        }
        await this.setState({ name: location.name, address: location.address, coordinates, categories: location.categories })
    }
    getLocationPermission = async () => {
        const { AppStore } = this.props
        AppStore.setLoading();
        try {

            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                // alert("You can use the location")
            }
            else {
                // alert("Location permission denied")
            }
        }
        catch (err) {
            console.warn(err)
        }
        this.watchID = navigator.geolocation.getCurrentPosition(
            async (position) => {
                if (!this.state.mode) {
                    this.setState({
                        coordinates: {
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude,
                            latitudeDelta: Math.abs(position.coords.latitude / 5000),
                            longitudeDelta: Math.abs(position.coords.longitude / 5000),
                            accuracy: position.coords.accuracy
                        },
                        error: null,
                    });
                    await this.getAddressByCoordinates({ lat: position.coords.latitude, lng: position.coords.longitude })
                }
                AppStore.setLoading();



            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: false, timeout: 50000, maximumAge: 1000 },
        );

    }

    onSelectedItemsChange = (categories) => {
        this.setState({ categories });
    }

    onPressMap = (e) => {
        this.setState({ coordinates: { ...this.state.coordinates, longitude: e.nativeEvent.coordinate.longitude, latitude: e.nativeEvent.coordinate.latitude } })
        this.getAddressByCoordinates({ lat: e.nativeEvent.coordinate.latitude, lng: e.nativeEvent.coordinate.longitude })

    }

    getAddressByCoordinates = async (coordinates) => {
        try {
            let response = await Geocoder.geocodePosition(coordinates)
            if (response.length > 0) {
                this.setState({ address: response[0].formattedAddress })
            }


        } catch (e) {
            console.log(e)
        }
    }


    handleSubmit = async () => {
        const { editMode } = this.state
        let valid = await this.validateForm();
        if (valid) {
            if (editMode) {
                this.updateLocation()
            } else {
                this.addLocation()
            }
        }
    }


    addLocation = async () => {
        const { LocationsStore } = this.props
        const { name, address, coordinates, categories } = this.state

        let location = {
            name,
            address,
            coordinates: {
                latitude: coordinates.latitude,
                longitude: coordinates.longitude
            },
            categories
        }
        LocationsStore.add(location)
        this.navigateToTabs()

    }



    updateLocation = async () => {
        const { id, name, address, coordinates, categories } = this.state

        let location = {
            id,
            name,
            address,
            coordinates: {
                latitude: coordinates.latitude,
                longitude: coordinates.longitude
            },
            categories
        }
        const { LocationsStore } = this.props
        LocationsStore.update(location)
        this.navigateToTabs()
    }


    removeLocation = () => {
        const { id } = this.state
        const { LocationsStore } = this.props
        LocationsStore.remove(id)
    }

    validateForm = async () => {
        const { name, address, coordinates, categories } = this.state
        let valid = this.state.valid;
        _.forOwn(valid, (value, key) => {
            if (key.startsWith('error')) valid[key] = false;
        });
        await this.setState({ valid });
        let validation = new customValidator({
            name,
            address,
            latitude: coordinates.latitude,
            longitude: coordinates.longitude,
            categories
        }, locationForm);


        validation.passes();
        validation.fails();

        valid = _.cloneDeep(this.state.valid);
        _.forOwn(validation.errors.all(), (value, key) => {
            valid[`error_${key}`] = value[0];
        });
        await this.setState({ valid });
        return validation.passes()
    }

    navigateToTabs = () => {
        // AppStore.reset('TabStack')
        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName: 'TabStack',
                    action: NavigationActions.navigate({
                        routeName: 'Locations', // Navigate to this screen
                    }),
                })
            ],
            key: null // THIS LINE
            ,
        });
        this.props.navigation.dispatch(resetAction);
    }
    render() {

        const { container, viewForm, viewIcon, viewTextInput, viewTextFormInput, viewMap, containerMap, map, txtInputName, chipTextStyle, chipContainerStyle, viewSectionList, viewBtnSubmit, btnSubmit, txtBtnSubmit, viewDetailsMap, txtDetailMap, viewErrorMessage, txtErrorMsg } = Styles
        const { Categories } = this.props.CategoriesStore
        const { editMode, name, address } = this.state
        return (
            <View style={container}>
                <View style={viewForm}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={viewTextFormInput}>
                            <View style={viewTextInput}>
                                <TextInput
                                    caretHidden={true}
                                    style={txtInputName}
                                    underlineColorAndroid='transparent'
                                    onChangeText={(name) => this.setState({ name, valid: {} })}
                                    value={name}
                                    placeholder={'name'}
                                />
                            </View>
                            <View style={viewErrorMessage}>
                                {this.state.valid && this.state.valid.error_name && <Text style={txtErrorMsg}> {this.state.valid.error_name}</Text>}
                            </View>
                        </View>
                        <View style={viewTextFormInput}>
                            <View style={viewTextInput}>
                                <TextInput
                                    caretHidden={true}
                                    style={txtInputName}
                                    underlineColorAndroid='transparent'
                                    onChangeText={(address) => this.setState({ address, valid: {} })}
                                    value={address}
                                    placeholder={'address'}
                                />
                            </View>
                            <View style={viewErrorMessage}>
                                {this.state.valid && this.state.valid.error_address && <Text style={txtErrorMsg}> {this.state.valid.error_address}</Text>}
                            </View>
                        </View>


                        <View style={viewMap}>
                            <MapView style={map}
                                region={this.state.coordinates}

                                onUserLocationChange={(e) => { 'onUserLocation', console.log(e.nativeEvent) }}
                                // onPress={(e) => { this.setState({  longitude: e.nativeEvent.coordinate.longitude, latitude: e.nativeEvent.coordinate.latitude }) }}
                                onPress={this.onPressMap}
                                onCalloutPress={(e) => { 'onCalloutPress', console.log(e.nativeEvent) }}


                            >

                                {this.state.coordinates.latitude != 0 && this.state.coordinates.longitude != 0 && <MapView.Marker
                                    coordinate={{ "latitude": this.state.coordinates.latitude, "longitude": this.state.coordinates.longitude }}
                                    title={"Your Location"}
                                />}

                            </MapView>
                            <View style={viewDetailsMap}>
                                <Text style={txtDetailMap}>latitude: {this.state.coordinates.latitude}</Text>
                                <Text style={txtDetailMap}>longitude: {this.state.coordinates.longitude}</Text>

                            </View>



                        </View>
                        <View style={viewErrorMessage}>
                            {this.state.valid && this.state.valid.error_latitude && <Text style={txtErrorMsg}> {this.state.valid.error_latitude}</Text>}
                            {this.state.valid && this.state.valid.error_longitude && <Text style={txtErrorMsg}> {this.state.valid.error_longitude}</Text>}

                        </View>
                        <View style={viewSectionList}>
                            <SectionedMultiSelect
                                items={Categories}
                                uniqueKey='id'
                                selectText='Choose some categories...'
                                showDropDowns={true}
                                onSelectedItemsChange={this.onSelectedItemsChange}
                                selectedItems={this.state.categories}
                                styles={{
                                    itemBackground: {
                                        backgroundColor: colors.purple
                                    },
                                    chipContainer: chipContainerStyle,
                                    chipText: chipTextStyle,
                                    selectToggleText: chipTextStyle,
                                    searchPlaceholderTextColor: colors.dark_blue
                                }}
                            />
                            <View style={viewErrorMessage}>
                                {this.state.valid && this.state.valid.error_categories && <Text style={txtErrorMsg}> {this.state.valid.error_categories}</Text>}
                            </View>


                        </View>


                        <View style={viewBtnSubmit}>
                            <Button style={btnSubmit} onPress={() => { this.handleSubmit() }}>

                                <Text style={txtBtnSubmit}>{editMode ? 'Edit Location' : 'Add Location'}</Text>
                            </Button>

                        </View>

                    </ScrollView>









                </View>
            </View>
        )
    }
}

export default AddLocationScreen