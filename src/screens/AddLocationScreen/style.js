import { StyleSheet, Dimensions } from 'react-native'
import { calcSize, DIMENSIONS, EDGE_DEVICE } from '../../utils/Consts'
import colors from '../../utils/Colors'
import Fonts from '../../utils/Fonts';

const Styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: colors.grey,
        alignItems: 'center'
    },
    viewForm: {

        paddingVertical:calcSize(50),
        backgroundColor: colors.white,
        margin: calcSize(20),
        width: DIMENSIONS.width - calcSize(30),
        minHeight: DIMENSIONS.height - calcSize(250),
        borderRadius: calcSize(20),
        alignItems: 'center'

    },
    viewTextFormInput: {
        margin: calcSize(10),
        alignItems: 'center',
        justifyContent: 'center'
    },
    viewTextInput: {
        width: calcSize(500),
        height: calcSize(80),
        backgroundColor: colors.light_grey,
    },
    txtInputName: {
        fontFamily: Fonts.medium,
        fontSize: calcSize(28),
    },
    viewIcon: {
        backgroundColor: colors.light_grey,
        width: calcSize(100),
        height: calcSize(100),
        borderRadius: calcSize(50),
        justifyContent: 'center',
        alignItems: 'center',
        margin:calcSize(20)
    },
    viewErrorMessage: {
        justifyContent:'center',
        alignItems:'center',
        marginTop: calcSize(5),

    },
    txtErrorMsg: {
        fontFamily: Fonts.light,
        fontSize: calcSize(22),
        color: colors.purple
    },
    viewMap: {
        height: calcSize(500),
        width: DIMENSIONS.width - calcSize(150),
        marginTop:calcSize(20),
    },
    containerMap: {
        ...StyleSheet.absoluteFillObject,
        height: calcSize(500),
        width: DIMENSIONS.width - calcSize(150),
        borderRadius:calcSize(20),
        justifyContent: 'flex-end',
        alignItems: 'center',
      },
      map: {
        ...StyleSheet.absoluteFillObject,
      },
      chipTextStyle:{
        fontFamily: Fonts.medium,
        fontSize: calcSize(24),
        color:colors.purple
      },
      chipContainerStyle:{
        backgroundColor:colors.yellow,
        borderColor:colors.purple,
        borderWidth:calcSize(3)
      },
      viewSectionList:{
          marginTop:calcSize(10),
          minHeight:calcSize(100),
          width:DIMENSIONS.width- calcSize(150),
       
      },
      viewBtnSubmit:{
        marginTop: calcSize(30),
        justifyContent:'center',
        alignItems:'center'
    },
    btnSubmit:{
        backgroundColor:colors.green,
        width:calcSize(200),
        height:calcSize(80),
        borderRadius:calcSize(10),
        justifyContent:'center',
        alignItems:'center'
    },
    txtBtnSubmit:{
        fontFamily:Fonts.medium,
        fontSize: calcSize(20),
        color:colors.white
    },
    viewDetailsMap:{
        backgroundColor:colors.transparent_a,
        height:calcSize(150),
        minWidth:calcSize(300),
        justifyContent:'center',
        alignItems:'center',
        borderRadius:calcSize(30),
        position:'absolute',
        bottom:0,
        right:0
    },
    txtDetailMap:{
        fontFamily:Fonts.medium,
        fontSize: calcSize(20),
        color:colors.purple
    }








})

export default Styles
