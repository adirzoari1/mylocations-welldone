import {StyleSheet,Dimensions} from 'react-native'
import { calcSize, DIMENSIONS, EDGE_DEVICE } from '../../utils/Consts'
import colors from '../../utils/Colors'
import Fonts from '../../utils/Fonts';

const Styles = StyleSheet.create({
 
    container:{
        flex:1,
        backgroundColor:colors.grey,
        justifyContent:'center',
        alignItems:'center'
    },
    viewForm:{
        backgroundColor:colors.white,
        marginTop:calcSize(20),
        width:DIMENSIONS.width - calcSize(30),
        height:DIMENSIONS.height/2,
        borderRadius:calcSize(20),
        justifyContent:'space-around',
        alignItems:'center'

    },
    viewTextFormInput:{
        alignItems:'center',
        justifyContent:'center'
    },
    viewTextInput:{
        width:calcSize(650),
        backgroundColor:colors.light_grey,
        height:calcSize(100),
    },
    txtInputName:{
        fontFamily:Fonts.medium,
        fontSize: calcSize(28),
    },
    viewBtnSubmit:{
        marginTop: calcSize(30),
    },
    btnSubmit:{
        backgroundColor:colors.green,
        width:calcSize(300),
        height:calcSize(100),
        borderRadius:calcSize(20),
        justifyContent:'center',
        alignItems:'center'
    },
    txtBtnSubmit:{
        fontFamily:Fonts.medium,
        fontSize: calcSize(23),
        color:colors.white
    },
    viewIcon:{
        backgroundColor:colors.light_grey,
        width: calcSize(160),
        height:calcSize(160),
        borderRadius:calcSize(80),
        justifyContent:'center',
        alignItems:'center'
    },
    viewErrorMessage:{
        marginTop:calcSize(12),
      
    },
    txtErrorMsg:{
        fontFamily:Fonts.light,
        fontSize: calcSize(22),
        color:colors.purple
    }







    

})

export default Styles
