import React, { Component } from 'react'
import {
    View,
    Text,
    ImageBackground,
    Image,
    TextInput
} from 'react-native'

import { inject, observer } from 'mobx-react/native'
import Icon from 'react-native-vector-icons/Feather';
// import moment from 'moment';

import Styles from './style'
import _ from 'lodash'
import Button from '../../components/Button/Button';
import { calcSize } from '../../utils/Consts';
import { StackActions, NavigationActions } from 'react-navigation';
import colors from '../../utils/Colors';
import { categoriesForm, customValidator } from '../../utils/Validator';

// import * as Animatable from 'react-native-animatable';

@inject('CategoriesStore','AppStore') @observer
class AddCategoryScreen extends Component {



    constructor(props) {
        super(props)
        this.state = {
            id: '',
            name: '',
            editMode: false,
            valid: {}

        }


    }


    componentDidMount = async () => {
        if (this.props.navigation.state.params && this.props.navigation.state.params.id >= 0) {
            this.setState({ id: this.props.navigation.state.params.id, editMode: true })
            await this.getCategoryById(this.props.navigation.state.params.id)

        }


    }

    getCategoryById = (id) => {
        const { CategoriesStore } = this.props
        let category = CategoriesStore.getCategoryById(id);
        this.setState({ name: category.name })


    }



    addCategory = () => {
        const { CategoriesStore } = this.props
        CategoriesStore.add(this.state.name)
       
        this.navigateToTabs();

    }
    updateCategory = () => {
        const { id, name } = this.state
        const { CategoriesStore,AppStore } = this.props
        CategoriesStore.update({ id, name })
        this.navigateToTabs()
    }
    navigateToTabs = () => {
            const { AppStore } = this.props
            // AppStore.reset('TabStack')
        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ 
                    routeName: 'TabStack',
                    action: NavigationActions.navigate({
                        routeName: 'Categories', // Navigate to this screen
                      }),
                })
            ],
            key: null // THIS LINE
            ,
        });
        this.props.navigation.dispatch(resetAction);
    }
    removeCategory = () => {
        const { id } = this.state
        const { CategoriesStore } = this.props
        CategoriesStore.remove(id)
    }
    handleSubmit = async () => {
        const { editMode } = this.state
        let valid = await this.validateForm();
        if (valid) {
            if (editMode) {
                this.updateCategory()
            } else {
                this.addCategory()
            }
        }
    }

    validateForm = async () => {
        const { name } = this.state
        let valid = this.state.valid;
        _.forOwn(valid, (value, key) => {
            if (key.startsWith('error')) valid[key] = false;
        });
        await this.setState({ valid });
        let validation = new customValidator({
            name

        }, categoriesForm);


        validation.passes();
        validation.fails();

        valid = _.cloneDeep(this.state.valid);
        _.forOwn(validation.errors.all(), (value, key) => {
            valid[`error_${key}`] = value[0];
        });
        await this.setState({ valid });
        return validation.passes()
    }


    render() {

        const { container, viewForm, viewTextFormInput,viewTextInput, txtInputName, viewBtnSubmit, btnSubmit, txtBtnSubmit, viewIcon, viewErrorMessage, txtErrorMsg } = Styles
        const { CategoriesStore } = this.props
        const { name, editMode } = this.state

        return (
            <View style={container}>
                <View style={viewForm}>
                    <View style={viewIcon}>
                        {editMode ?
                            <Icon name='edit' size={calcSize(100)} color={colors.purple} /> :
                            <Icon name='plus' size={calcSize(100)} color={colors.purple} />
                        }
                    </View>
                    <View style={viewTextFormInput}>
                        <View style={viewTextInput}>
                            <TextInput
                                caretHidden={true}
                                style={txtInputName}
                                underlineColorAndroid='transparent'
                                onChangeText={(name) => this.setState({ name,valid:{} })}
                                value={name}
                                placeholder={'category'}
                            />
                          
                        </View>
                        <View style={viewErrorMessage}>
                                {this.state.valid && this.state.valid.error_name && <Text style={txtErrorMsg}> {this.state.valid.error_name}</Text>}
                            </View>
                    </View>

                    <View style={viewBtnSubmit}>
                        <Button style={btnSubmit} onPress={() => { this.handleSubmit() }}>

                            <Text style={txtBtnSubmit}>{editMode ? 'Edit Category' : 'Add Category'}</Text>
                        </Button>

                    </View>
                </View>
              



            </View>

        )
    }
}

export default AddCategoryScreen