/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, PermissionsAndroid } from 'react-native';
import MapView, { PROVIDER_GOOGLE,Marker } from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
import { inject, observer } from 'mobx-react/native'
@observer
class App extends Component<Props> {

  constructor(props) {
    super(props)
    this.state = {
      region: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0
      },
      latitude: 0,
      longitude: 0,
      error:null,
    }
  }

  async componentDidMount() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        alert("You can use the location")
      }
      else {
        alert("Location permission denied")
      }
    }
    catch (err) {
      console.warn(err)
    }
    this.watchID = navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: Math.abs(position.coords.latitude/5000),
            longitudeDelta: Math.abs(position.coords.longitude/5000),
            accuracy: position.coords.accuracy
          },
          error: null,
        });
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
    );
  }
  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }


 
  render() {
    console.log('this.tstate',this.state)
    return (
      <View style={{ flex: 1 }}>
         <MapView style={styles.map} 
                region={this.state.region}

      onUserLocationChange = {(e)=>{'onUserLocation',console.log(e.nativeEvent)}}
      onPress = {(e)=>{ this.setState({longitude:e.nativeEvent.coordinate.longitude, latitude: e.nativeEvent.coordinate.latitude})}}
      onCalloutPress = {(e)=>{'onCalloutPress',console.log(e.nativeEvent)}}
     

      >
  
      {this.state.region.latitude!=0 && this.state.region.longitude!=0 && <MapView.Marker
         coordinate={{"latitude":this.state.region.latitude,"longitude":this.state.region.longitude}}
         title={"Your Location"}
       />}

      </MapView>
           
      </View>
    );
  }
}
export default App;
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
